<?php 
/*----------------------------------------------------------------*\

	LEAGUE ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php $image = get_field('header_image', 'options'); ?>
<header class="post-head <?php if( !$image ) :?>has-no-image<?php endif; ?>" <?php if( $image ) :?>style="background-image: linear-gradient(rgba(119,22,70,0.5), rgba(119,22,70,0.5)), url(<?php echo $image['sizes']['xlarge']; ?>);"<?php endif; ?>>
	
	<?php $video = get_field('header_video', 'options'); ?>
	<?php $headerimage = get_field('header_image', 'options'); ?>
	<?php if( get_field('header_video', 'options') ) :?>
		<video muted="" autoplay="" loop="" playsinline="" class="video" poster="<?php echo $headerimage['url']; ?>" src="<?php echo $video['url']; ?>"></video>
		<div class="overlay"></div>
	<?php endif; ?>

	<?php if( get_field('header_title', 'options') ) :?>
		<h1><?php the_field('header_title', 'options'); ?></h1>
	<?php else : ?>
		<h1><?php the_title(); ?></h1>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<div class="standard">
			<h2>Find a League</h2>
			<p>Shaffer Entertainment makes it easy to join a league. Choose your sport, pick a location, check out the league standings and schedule.</p>
			<a class="button" href="<?php the_field('important_dates', 'options'); ?>">Important Dates</a>
			<a class="button" href="/league-rules/">League Rules</a>
			<a class="button" href="/nado-rewards/">NADO Ratings</a>
			<a class="button" href="https://lms.fargorate.com/PublicReport/LeagueReports?leagueId=c09d7b60-4289-4cbf-a74b-a8f60166df08" target="_blank">Stats & Schedules</a>
			<!-- <a class="button" href="/2020-columbus-spring-darts/?sport=soft-tip-darts&day=day%20varies&league-name=2020-spring-darts&bar=uncategorized">2020 Spring Darts</a> -->
			<br>
			<form action="/leagues/#bars" method="get">
				<?php
					$sport = $_GET['sport'];
				?>
				<h2 class="blue">1. Choose your sport</h2>
				<?php $terms = get_terms( array(
					'taxonomy' => 'sport',
					'hide_empty' => false,
				) ); ?>
				<?php foreach ( $terms as $term ) { ?>
					<input type="radio" name="sport" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php if( $term->slug == $sport ) : ?>checked="checked"<?php endif; ?> />
					<label for="<?php echo $term->slug; ?>">
						<svg>
							<use xlink:href="#golf-icon" />
						</svg>
						<svg>
							<use xlink:href="#pool-icon" />
						</svg>
						<svg>
							<use xlink:href="#bowling-icon" />
						</svg>
						<svg>
							<use xlink:href="#darts-icon" />
						</svg>
						<p><?php echo $term->name; ?></p>
					</label>
				<?php } ?>
				<?php
					$location = $_GET['location'];
				?>
				<h2 class="blue">2. Choose a location</h2>
				<?php $terms = get_terms( array(
					'taxonomy' => 'location',
					'hide_empty' => false,
				) ); ?>
				<?php foreach ( $terms as $term ) { ?>
					<input type="radio" name="location" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php if( $term->slug == $location ) : ?>checked="checked"<?php endif; ?> />
					<label for="<?php echo $term->slug; ?>">
						<svg>
							<use xlink:href="#location-marker" />
						</svg>
						<p><?php echo $term->name; ?></p>
					</label>
				<?php } ?>
				<?php
					$bar = $_GET['bar'];
				?>
				<?php 
					$locationslug = get_term_by('slug', $location, 'location');
					$locationid = $locationslug->term_id;
				?>
				<h1><?php echo $locationslug->id; ?></h1>
				<?php $terms = get_terms( array(
					'taxonomy' => 'bar',
					'hide_empty' => false,
					'meta_key'		=> 'location',
					'meta_value'	=> $locationid,
				) ); ?>
				<div id="bars">

					<?php if( $location == 'indianapolis-in' && $sport == 'soft-tip-darts' ) : ?>
						<?php if( get_field('indianapolis_dart_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('indianapolis_dart_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'indianapolis-in' && $sport == 'silver-strike-bowling' ) : ?>
						<?php if( get_field('indianapolis_bowling_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('indianapolis_bowling_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'indianapolis-in' && $sport == 'pool' ) : ?>
						<?php if( get_field('indianapolis_pool_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('indianapolis_pool_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'indianapolis-in' && $sport == 'golden-tee' ) : ?>
						<?php if( get_field('indianapolis_golf_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('indianapolis_golf_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'columbus-oh' && $sport == 'soft-tip-darts' ) : ?>
						<?php if( get_field('columbus_dart_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('columbus_dart_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'columbus-oh' && $sport == 'silver-strike-bowling' ) : ?>
						<?php if( get_field('columbus_bowling_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('columbus_bowling_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'columbus-oh' && $sport == 'pool' ) : ?>
						<?php if( get_field('columbus_pool_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('columbus_pool_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'columbus-oh' && $sport == 'golden-tee' ) : ?>
						<?php if( get_field('columbus_golf_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('columbus_golf_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'livonia-mi' && $sport == 'soft-tip-darts' ) : ?>
						<?php if( get_field('livonia_dart_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('livonia_dart_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'livonia-mi' && $sport == 'silver-strike-bowling' ) : ?>
						<?php if( get_field('livonia_bowling_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('livonia_bowling_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'livonia-mi' && $sport == 'pool' ) : ?>
						<?php if( get_field('livonia_pool_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('livonia_pool_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'livonia-mi' && $sport == 'golden-tee' ) : ?>
						<?php if( get_field('livonia_golf_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('livonia_golf_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'richmond-in' && $sport == 'soft-tip-darts' ) : ?>
						<?php if( get_field('richmond_dart_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('richmond_dart_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'richmond-in' && $sport == 'silver-strike-bowling' ) : ?>
						<?php if( get_field('richmond_bowling_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('richmond_bowling_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'richmond-in' && $sport == 'pool' ) : ?>
						<?php if( get_field('richmond_pool_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('richmond_pool_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php elseif( $location == 'richmond-in' && $sport == 'golden-tee' ) : ?>
						<?php if( get_field('richmond_golf_ratings', 'options') ): ?>
							<a class="button" target="_blank" href="<?php the_field('richmond_golf_ratings', 'options'); ?>">Ratings</a>
						<?php endif; ?>
					<?php endif; ?>

					<label class="select-label" name="bar">Where do you want to play?</label>
					<select name="bar" class="all-bars">
						<option value="">Select bar�</option>
						<?php foreach ( $terms as $term ) { ?>
								<option value="<?php echo $term->slug; ?>" <?php if( $term->slug == $bar ) : ?>selected<?php endif; ?>><?php echo $term->name; ?></option>
						<?php } ?>
					</select>
				</div>
				<input class="button" type="submit" value="Find Leagues" />
			</form>
		</div>
		<section id="league" class="standard">
			<h2 class="blue">3. Choose a league</h2>
			<?php if ( have_posts() ) : ?>
				<h3>Sunday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Sunday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Monday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Monday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Tuesday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Tuesday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Wednesday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Wednesday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Thursday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Thursday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Friday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Friday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Saturday</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Saturday' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<h3>Day Varies</h3>
				<div>
					<?php	while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field( 'day' ) == 'Day Varies' ) : ?>
							<article class="archive-result <?php echo $post_type; ?>">
								<header>
									<h5><?php the_title(); ?></h5>
								</header>
								<div class="entry-content">
									<div>
										<?php $schedulelink = get_field('schedule_link'); ?>
										<?php if( $schedulelink ): ?>
											<a class="button is-blue" href="<?php the_field('schedule_link'); ?>" target="_blank">Schedule</a>
										<?php endif; ?>
										<?php $standingslink = get_field('standings_link'); ?>
										<?php if( $standingslink ): ?>
											<a class="button is-red" href="<?php the_field('standings_link'); ?>" target="_blank">Standings</a>
										<?php endif; ?>
									</div>
									<?php $signuplink = get_field('sign_up_link'); ?>
									<?php if( $signuplink ): ?>
									<?php
										$sport = get_field('sport');
										$sport_term = get_term($sport);
										$sport_slug = $sport_term->slug;
										$day = get_field('day');
										$league = get_the_title();
										$bar = get_field('bar');
										$bar_term = get_term($bar);
										$bar_slug = $bar_term->slug;
									?>
									<p>Interested in joining this league? <a href="<?php echo $signuplink['url']; ?>?sport=<?php echo $sport_slug; ?>&day=<?php echo strtolower($day); ?>&league-name=<?php echo str_replace(' ', '-', strtolower($league)); ?>&bar=<?php echo $bar_slug; ?>" target="<?php echo $signuplink['target']; ?>">Sign Up</a></p>
									<?php endif; ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			<?php else : ?>
				<article>
					<section class="is-narrow">
						<p>Darn, nothing found at this bar. Try another one?</p>
					</section>
				</article>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</section>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>