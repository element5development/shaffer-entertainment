<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying the locations

\*----------------------------------------------------------------*/
?>
<?php if( get_sub_field('show_locations') ): ?>
<section class="locations is-extra-wide">
	<h2>Locations</h2>
	<?php if( get_field('location_repeater','options') ): ?>
		<?php while ( have_rows('location_repeater','options') ) : the_row(); ?>
			<?php 
				$phone_link = get_sub_field('phone','options');
				$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
			?>
			<article>
				<?php the_sub_field('map','options'); ?>
				<h3><?php the_sub_field('location','options'); ?></h3>
				<p><?php the_sub_field('address','options'); ?><br>
				Phone: <a href="tel:+1<?php echo $phone_link; ?>"><?php the_sub_field('phone','options'); ?></a><br>
				<?php if( get_sub_field('fax','options') ) : ?>Fax: <?php the_sub_field('fax','options'); ?></p><?php endif; ?>
			</article>
		<?php endwhile; ?>
	<?php endif; ?>
</section>
<?php endif; ?>