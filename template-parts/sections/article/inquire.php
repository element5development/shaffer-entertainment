<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying the inquire section

\*----------------------------------------------------------------*/
?>
<section class="inquire is-extra-wide">
	<div>
		<h3><?php the_sub_field('inquire_section_title'); ?></h3>
		<?php $link = get_sub_field('inquire_button'); ?>
		<?php if( $link ): ?>
			<a class="button is-white" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
	<div <?php if( get_sub_field('number_of_images') == 'Ten' ): ?>class="has-ten"<?php endif; ?>>
		<?php if( get_sub_field('number_of_images') == 'Four' ): ?>
			<?php $image = get_sub_field('image_1'); ?>
			<img class="four lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			<?php $image = get_sub_field('image_2'); ?>
			<img class="four lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			<?php $image = get_sub_field('image_3'); ?>
			<img class="four lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			<?php $image = get_sub_field('image_4'); ?>
			<img class="four lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
		<?php elseif( get_sub_field('number_of_images') == 'Ten' ): ?>
			<?php $image = get_sub_field('image_1'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_2'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_3'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_4'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_5'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_6'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_7'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_8'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_9'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
			<?php $image = get_sub_field('image_10'); ?>
			<div class="ten">
				<img class="lazyload blur-up" data-expand="-100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
			</div>
		<?php endif; ?>
	</div>
</section>