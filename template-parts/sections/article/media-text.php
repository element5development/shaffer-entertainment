<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with an editor

\*----------------------------------------------------------------*/
?>

<?php if ( get_sub_field('image_size')  == 'image-cover' ) : ?>
	<section class="media-text <?php the_sub_field('width'); ?> <?php the_sub_field('image_alignment'); ?> <?php the_sub_field('image_size') ?>">
		<?php $image = get_sub_field('image'); ?>
		<div style="background-image: url(<?php echo $image['sizes']['medium']; ?>);">
		</div>
		<div>
			<?php the_sub_field('content'); ?>
		</div>
	</section>
<?php else : ?>
	<section class="media-text <?php the_sub_field('width'); ?> <?php the_sub_field('image_alignment'); ?> <?php the_sub_field('image_size') ?>">
		<div>
			<?php $image = get_sub_field('image'); ?>
			<img class="lazyload blur-up" data-expand="-250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 500w, <?php echo $image['sizes']['medium']; ?> 1024w, <?php echo $image['sizes']['large']; ?> 1200w, <?php echo $image['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $image['alt']; ?>">
		</div>
		<div>
			<?php the_sub_field('content'); ?>
		</div>
	</section>
<?php endif; ?>