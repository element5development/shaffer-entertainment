<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying the team repeater

\*----------------------------------------------------------------*/
?>
<?php if( get_sub_field('team_repeater') ): ?>
<section class="team-repeater is-extra-wide">
	<h3><?php the_sub_field('team_section_title'); ?></h3>
	<div class="choose-location">
		<button class="show-columbus is-active">Columbus, OH</button>
		<button class="show-indianapolis">Indianapolis, IN</button>
		<button class="show-livonia">Livonia, MI</button>
	</div>
	<div>
		<div class="columbus is-active">
			<?php while ( have_rows('team_repeater') ) : the_row(); ?>
				<?php if( get_sub_field('location') == 'Columbus' ): ?>
					<article>
						<a href="mailto:<?php the_sub_field('email'); ?>" target="_blank"></a>
						<?php $image = get_sub_field('headshot'); ?>
						<?php if( $image ): ?>
						<div class="featured-image">
							<img class="lazyload blur-up" data-expand="-50" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
							<div class="overlay"></div>
							<div class="email-icon"></div>
						</div>
						<?php endif; ?>
						<h4><?php the_sub_field('name'); ?></h4>
						<p><?php the_sub_field('job_title'); ?></p>
					</article>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
		<div class="indianapolis">
			<?php while ( have_rows('team_repeater') ) : the_row(); ?>
				<?php if( get_sub_field('location') == 'Indianapolis' ): ?>
					<article>
						<a href="mailto:<?php the_sub_field('email'); ?>" target="_blank"></a>
						<?php $image = get_sub_field('headshot'); ?>
						<?php if( $image ): ?>
						<div class="featured-image">
							<img class="lazyload blur-up" data-expand="-50" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
							<div class="overlay"></div>
							<div class="email-icon"></div>
						</div>
						<?php endif; ?>
						<h4><?php the_sub_field('name'); ?></h4>
						<p><?php the_sub_field('job_title'); ?></p>
					</article>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
		<div class="livonia">
			<?php while ( have_rows('team_repeater') ) : the_row(); ?>
				<?php if( get_sub_field('location') == 'Livonia' ): ?>
					<article>
						<a href="mailto:<?php the_sub_field('email'); ?>" target="_blank"></a>
						<?php $image = get_sub_field('headshot'); ?>
						<?php if( $image ): ?>
						<div class="featured-image">
							<img class="lazyload blur-up" data-expand="-50" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
							<div class="overlay"></div>
							<div class="email-icon"></div>
						</div>
						<?php endif; ?>
						<h4><?php the_sub_field('name'); ?></h4>
						<p><?php the_sub_field('job_title'); ?></p>
					</article>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>