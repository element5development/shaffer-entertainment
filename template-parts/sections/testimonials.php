<?php 
/*----------------------------------------------------------------*\

	TESTIMONIALS

\*----------------------------------------------------------------*/
?>
<?php if( get_field('testimonial_repeater') ): ?>
<section class="testimonials is-extra-wide">
	<h2>See what our clients have to say:</h2>
	<div class="testimonial-container">
		<?php while ( have_rows('testimonial_repeater') ) : the_row(); ?>
			<article class="testimonial">
				<button href="<?php the_sub_field('testimonial_video'); ?>?modestbranding=1&autoplay=0&fs=1&showinfo=0&rel=0" data-featherlight="iframe" data-featherlight-iframe-width="1000" data-featherlight-iframe-height="600" data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media" data-featherlight-iframe-allowfullscreen="true">
					<img src="<?php the_sub_field('testimonial_image'); ?>" alt="<?php the_sub_field('testimonial_image'); ?>"/>
					<svg>
						<use xlink:href="#play-button" />
					</svg>
				</button>
			</article>
		<?php endwhile; ?>
	</div>
</section>
<hr>
<?php endif; ?>