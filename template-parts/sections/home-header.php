<?php 
/*----------------------------------------------------------------*\

	HOME HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>
<?php $image = get_field('header_image'); ?>
<header class="post-head <?php if( !$image ) :?>has-no-image<?php endif; ?>" <?php if( $image ) :?>style="background-image: linear-gradient(rgba(119,22,70,0.5), rgba(119,22,70,0.5)), url(<?php echo $image['sizes']['xlarge']; ?>);"<?php endif; ?>>
	
	<?php $video = get_field('header_video'); ?>
	<?php $headerimage = get_field('header_image'); ?>
	<?php if( get_field('header_video') ) :?>
		<video muted="" autoplay="" loop="" playsinline="" class="video" poster="<?php echo $headerimage['url']; ?>" src="<?php echo $video['url']; ?>"></video>
		<div class="overlay"></div>
	<?php endif; ?>

	<?php if( get_field('header_title') ) :?>
		<h1><?php the_field('header_title'); ?></h1>
	<?php else : ?>
		<h1><?php the_title(); ?></h1>
	<?php endif; ?>
</header>
<section class="three-boxes">
	<div>
		<h2><?php the_field('left_box_title'); ?></h2>
		<?php $link = get_field('left_box_button'); ?>
		<?php if( $link ): ?>
			<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
	<div>
		<h2><?php the_field('center_box_title'); ?></h2>
		<?php $link2 = get_field('center_box_button'); ?>
		<?php if( $link2 ): ?>
			<a class="button" href="<?php echo $link2['url']; ?>" target="<?php echo $link2['target']; ?>"><?php echo $link2['title']; ?></a>
		<?php endif; ?>
	</div>
	<div>
		<h2><?php the_field('right_box_title'); ?></h2>
		<?php $link3 = get_field('right_box_button'); ?>
		<?php if( $link3 ): ?>
			<a class="button" href="<?php echo $link3['url']; ?>" target="<?php echo $link3['target']; ?>"><?php echo $link3['title']; ?></a>
		<?php endif; ?>
	</div>
</section>