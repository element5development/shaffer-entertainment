<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>
<?php $image = get_field('header_image'); ?>
<header class="post-head <?php if( get_field('smaller_header') ): ?>smaller-header<?php endif; ?> <?php if( !$image ) :?>has-no-image<?php endif; ?>" <?php if( $image ) :?>style="background-image: linear-gradient(rgba(119,22,70,0.5), rgba(119,22,70,0.5)), url(<?php echo $image['sizes']['xlarge']; ?>);"<?php endif; ?>>
	<?php if( get_field('header_title') ) :?>
		<h1><?php the_field('header_title'); ?></h1>
	<?php else : ?>
		<h1><?php the_title(); ?></h1>
	<?php endif; ?>
</header>