<?php 
/*----------------------------------------------------------------*\

	HOME CONTENT

\*----------------------------------------------------------------*/
?>

<section class="home-content is-extra-wide">
	<?php $image = get_field('left_image'); ?>
	<img class="lazyload blur-up" data-expand="-50" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
	<?php $image2 = get_field('right_image'); ?>
	<img class="lazyload blur-up" data-expand="-50" data-sizes="auto" src="<?php echo $image2['sizes']['placeholder']; ?>" data-src="<?php echo $image2['sizes']['medium']; ?>" data-srcset="<?php echo $image2['sizes']['small']; ?> 350w, <?php echo $image2['sizes']['medium']; ?> 750w, <?php echo $image2['sizes']['large']; ?> 1000w, <?php echo $image2['sizes']['xlarge']; ?> 1300w" alt="<?php echo $image2['alt']; ?>" />
	<div class="editor"><?php the_field('editor'); ?></div>
</section>