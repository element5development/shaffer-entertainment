<?php 
/*----------------------------------------------------------------*\

	INSTAGRAM

\*----------------------------------------------------------------*/
?>

<section class="instagram is-full-width">
	<h2>Follow us on Instagram</h2>
	<?php the_field('instagram_section'); ?>
</section>