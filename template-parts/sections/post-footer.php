<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<h3>Love Shaffer Entertaiment? Give us a review!</h3>
		<div class="review-links">
			<a href="<?php the_field('review_google', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#google-review" />
				</svg>
			</a>
			<a href="<?php the_field('review_facebook', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook-review" />
				</svg>
			</a>
			<a href="<?php the_field('review_yelp', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#yelp-review" />
				</svg>
			</a>
		</div>
	</div>
	<div>
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation' )); ?>
		<div>
			<a class="button" href="<?php echo get_site_url(); ?>/leagues/">League Information</a>
			<div class="social-links">
				<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
				<a href="<?php the_field('instagram', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#instagram" />
					</svg>
				</a>
				<a href="<?php the_field('youtube', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#youtube" />
					</svg>
				</a>
			</div>
		</div>
	</div>
	<div class="copyright">
		<p>©<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved. <a href="<?php echo get_home_url(); ?>/privacy-policy/">Privacy Policy.</a></p>
	</div>
</footer>
<div id="element5-credit">
	<a target="_blank" href="https://element5digital.com">
		<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
	</a>
</div>