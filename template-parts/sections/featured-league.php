<?php 
/*----------------------------------------------------------------*\

	FEATURED LEAGUE

\*----------------------------------------------------------------*/
?>

<section class="featured-league is-extra-wide">
	<div>
		<?php the_field('featured_league_content'); ?>
		<?php $link = get_field('featured_league_button'); ?>
		<?php if( $link ): ?>
			<a class="button is-red" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
	<?php $image = get_field('featured_league_image'); ?>
	<?php if( $image ): ?>
	<img class="lazyload blur-up" data-expand="-50" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1300w" alt="<?php echo $image['alt']; ?>" />
	<?php endif; ?>
	<?php $video = get_field('featured_league_video'); ?>
	<?php if( $video ): ?>
	<iframe width="560" height="315" src="<?php echo $video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<?php endif; ?>
</section>