<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation<?php if ( get_field('notification_bar_text', 'options') ) : ?> notification-on<?php endif; ?>">
	<nav>
		<div>
			<a class="button" href="<?php echo get_site_url(); ?>/leagues/">League<span>s</span><span> Information</span></a>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_left_nav' )); ?>
		</div>
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_right_nav' )); ?>
			<div class="social-links">
				<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
				<a href="<?php the_field('instagram', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#instagram" />
					</svg>
				</a>
				<a href="<?php the_field('youtube', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#youtube" />
					</svg>
				</a>
			</div>
			<button class="activate-search">
				<svg>
					<use xlink:href="#search" />
				</svg>
			</button>
			<button class="activate-menu">
				Menu
				<svg>
					<use xlink:href="#menu" />
				</svg>
			</button>
		</div>
		<div class="search-form">
			<?php echo get_search_form(); ?>
		</div>
		<div class="mobile-menu">
			<button class="close-menu">Close</button>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_left_nav' )); ?>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_right_nav' )); ?>
			<div class="social-links">
				<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
				<a href="<?php the_field('instagram', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#instagram" />
					</svg>
				</a>
				<a href="<?php the_field('youtube', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#youtube" />
					</svg>
				</a>
			</div>
		</div>
	</nav>
</div>