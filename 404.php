<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1>Oops! This page can't be found.</h1>
</header>

<main id="main-content">
	<article>
		<section class="is-narrow">
			<h2>We can't seem to find the page you're looking for.</h2>
			<a class="button is-red" href="<?php echo get_site_url(); ?>">Home</a>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>