<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_left_nav' => __( 'Primary Left' ),
		'primary_right_nav' => __( 'Primary Right' ),
		'footer_navigation' => __( 'Footer Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );