<?php
/*----------------------------------------------------------------*\
	INITIALIZE TAXONOMIES
\*----------------------------------------------------------------*/
// Register Taxonomy Sport
function create_sport_tax() {

	$labels = array(
		'name'              => _x( 'Sports', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Sport', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Sports', 'textdomain' ),
		'all_items'         => __( 'All Sports', 'textdomain' ),
		'parent_item'       => __( 'Parent Sport', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Sport:', 'textdomain' ),
		'edit_item'         => __( 'Edit Sport', 'textdomain' ),
		'update_item'       => __( 'Update Sport', 'textdomain' ),
		'add_new_item'      => __( 'Add New Sport', 'textdomain' ),
		'new_item_name'     => __( 'New Sport Name', 'textdomain' ),
		'menu_name'         => __( 'Sport', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => false,
		'meta_box_cb' => false,
		'show_admin_column' => true,
		'show_in_rest' => true,
	);
	register_taxonomy( 'sport', array('league'), $args );

}
add_action( 'init', 'create_sport_tax' );

// Register Taxonomy Location
function create_location_tax() {

	$labels = array(
		'name'              => _x( 'Locations', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Location', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Locations', 'textdomain' ),
		'all_items'         => __( 'All Locations', 'textdomain' ),
		'parent_item'       => __( 'Parent Location', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
		'edit_item'         => __( 'Edit Location', 'textdomain' ),
		'update_item'       => __( 'Update Location', 'textdomain' ),
		'add_new_item'      => __( 'Add New Location', 'textdomain' ),
		'new_item_name'     => __( 'New Location Name', 'textdomain' ),
		'menu_name'         => __( 'Location', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => false,
		'meta_box_cb' => false,
		'show_admin_column' => true,
		'show_in_rest' => true,
	);
	register_taxonomy( 'location', array('league'), $args );

}
add_action( 'init', 'create_location_tax' );

// Register Taxonomy Bar
function create_bar_tax() {

	$labels = array(
		'name'              => _x( 'Bars', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Bar', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Bars', 'textdomain' ),
		'all_items'         => __( 'All Bars', 'textdomain' ),
		'parent_item'       => __( 'Parent Bar', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Bar:', 'textdomain' ),
		'edit_item'         => __( 'Edit Bar', 'textdomain' ),
		'update_item'       => __( 'Update Bar', 'textdomain' ),
		'add_new_item'      => __( 'Add New Bar', 'textdomain' ),
		'new_item_name'     => __( 'New Bar Name', 'textdomain' ),
		'menu_name'         => __( 'Bar', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => false,
		'meta_box_cb' => false,
		'show_admin_column' => true,
		'show_in_rest' => true,
	);
	register_taxonomy( 'bar', array('league'), $args );

}
add_action( 'init', 'create_bar_tax' );