<?php
/*----------------------------------------------------------------*\
	INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Register Custom Post Type League
function create_league_cpt() {

	$labels = array(
		'name' => _x( 'Leagues', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'League', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Leagues', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'League', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'League Archives', 'textdomain' ),
		'attributes' => __( 'League Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent League:', 'textdomain' ),
		'all_items' => __( 'All Leagues', 'textdomain' ),
		'add_new_item' => __( 'Add New League', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New League', 'textdomain' ),
		'edit_item' => __( 'Edit League', 'textdomain' ),
		'update_item' => __( 'Update League', 'textdomain' ),
		'view_item' => __( 'View League', 'textdomain' ),
		'view_items' => __( 'View Leagues', 'textdomain' ),
		'search_items' => __( 'Search League', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into League', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this League', 'textdomain' ),
		'items_list' => __( 'Leagues list', 'textdomain' ),
		'items_list_navigation' => __( 'Leagues list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Leagues list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'League', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'leagues' ),
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'league', $args );

}
add_action( 'init', 'create_league_cpt', 0 );