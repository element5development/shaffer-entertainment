<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/home-header'); ?>

<main id="main-content">
	<?php get_template_part('template-parts/sections/home-content'); ?>
</main>

<?php get_template_part('template-parts/sections/testimonials'); ?>

<?php get_template_part('template-parts/sections/featured-league'); ?>

<?php get_template_part('template-parts/sections/instagram'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>