<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if ( get_field('message') ) : ?>
		<article>
			<section class="standard">
				<h2><?php the_field('message'); ?></h2>
			</section>
		</article>
		<?php $posts = get_field('previews'); ?>
		<?php if ( $posts ) : ?>
			<aside class="acf-related-posts is-wide">
				<?php foreach( $posts as $post): setup_postdata($post); ?>
					<article class="related-relationship <?php echo get_post_type(); ?>">
						<a class="button" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</article>
				<?php endforeach; ?>
			</aside>
			<?php wp_reset_postdata();?>
		<?php endif; ?>
	<?php else : ?>
		<article>
			<section>
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>