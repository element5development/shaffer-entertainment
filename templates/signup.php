<?php 
/*----------------------------------------------------------------*\

	Template Name: Signup

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<div class="is-narrow">
			<h2>League Information</h2>
			<article>
				<?php
					$sport = $_GET['sport'];
				?>
				<?php if ( $sport == 'golden-tee' ) : ?>
					<svg>
						<use xlink:href="#golf-icon" />
					</svg>
				<?php elseif ( $sport == 'pool' ) : ?>
					<svg>
						<use xlink:href="#pool-icon" />
					</svg>
				<?php elseif ( $sport == 'silver-strike-bowling' ) : ?>
					<svg>
						<use xlink:href="#bowling-icon" />
					</svg>
				<?php elseif ( $sport == 'soft-tip-darts' ) : ?>
					<svg>
						<use xlink:href="#darts-icon" />
					</svg>
				<?php endif; ?>
				<div>
					<?php
						$day = $_GET['day'];
						$league = $_GET['league-name'];
						$bar = $_GET['bar'];
					?>
					<h5><?php echo $day; ?></h5>
					<h3><?php echo str_replace('-', ' ', $league); ?></h3>
					<p><?php echo str_replace('-', ' ', $bar); ?></p>
				</div>
				<a class="button is-red" href="<?php echo get_site_url(); ?>/leagues/">Change</a>
			</article>
			<h2>Team Information</h2>
			<?php $formid = get_field('gravity_form_id'); ?>
			<?php echo do_shortcode( '[gravityform id="'. $formid .'" title="false" description="false"]' ); ?>
		</div>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>