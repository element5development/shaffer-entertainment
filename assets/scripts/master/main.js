var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('.ginput_container_fileupload').addClass("file-uploaded");
		});
	}

	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$("button.activate-search").click(function () {
		$(".search-form").addClass("is-active");
		$(".search-form form input").focus();
	});
	$(document).click(function (event) {
		//if you click on anything except the search input, search button, or search open button, close the search
		if (!$(event.target).closest(".search-form form input, .search-form form button, button.activate-search").length) {
			$(".search-form").removeClass("is-active");
		}
	});

	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("button.activate-menu").click(function () {
		$(".mobile-menu").addClass("is-active");
	});
	$("button.close-menu").click(function () {
		$(".mobile-menu").removeClass("is-active");
	});
	$(document).click(function (event) {
		//if you click on anything except within the open menu, close the menu
		if (!$(event.target).closest(".mobile-menu, button.activate-menu").length) {
			$(".mobile-menu").removeClass("is-active");
		}
	});

	/*----------------------------------------------------------------*\
  	TEAM LOCATION SELECT
	\*----------------------------------------------------------------*/
	$(".choose-location button.show-columbus").click(function () {
		$(this).addClass("is-active");
		$(".choose-location button.show-indianapolis").removeClass("is-active");
		$(".choose-location button.show-livonia").removeClass("is-active");
		$("section.team-repeater .columbus").addClass("is-active");
		$("section.team-repeater .indianapolis").removeClass("is-active");
		$("section.team-repeater .livonia").removeClass("is-active");
	});
	$(".choose-location button.show-indianapolis").click(function () {
		$(this).addClass("is-active");
		$(".choose-location button.show-columbus").removeClass("is-active");
		$(".choose-location button.show-livonia").removeClass("is-active");
		$("section.team-repeater .indianapolis").addClass("is-active");
		$("section.team-repeater .columbus").removeClass("is-active");
		$("section.team-repeater .livonia").removeClass("is-active");
	});
	$(".choose-location button.show-livonia").click(function () {
		$(this).addClass("is-active");
		$(".choose-location button.show-indianapolis").removeClass("is-active");
		$(".choose-location button.show-columbus").removeClass("is-active");
		$("section.team-repeater .livonia").addClass("is-active");
		$("section.team-repeater .columbus").removeClass("is-active");
		$("section.team-repeater .indianapolis").removeClass("is-active");
	});

	/*------------------------------------------------------------------
  	TESTIMONIAL SLIDER
	------------------------------------------------------------------*/
	$('.testimonial-container').slick({
		centerMode: true,
		arrows: true,
		centerPadding: '0px',
		slidesToShow: 3,
		prevArrow: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M24.19 29.82a1.18 1.18 0 0 1 .39.91 1.21 1.21 0 0 1-.39.91 1.29 1.29 0 0 1-1.79 0L7.82 16.91a1.24 1.24 0 0 1 0-1.82L22.4.36a1.29 1.29 0 0 1 1.79 0 1.3 1.3 0 0 1 .39.93 1.17 1.17 0 0 1-.39.89L10.9 16z" fill="#FF173A" data-name="Layer 1"/></svg>',
		nextArrow: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M7.81 2.18a1.18 1.18 0 0 1-.39-.91 1.21 1.21 0 0 1 .39-.91 1.29 1.29 0 0 1 1.79 0l14.58 14.73a1.24 1.24 0 0 1 0 1.82L9.6 31.64a1.29 1.29 0 0 1-1.79 0 1.3 1.3 0 0 1-.39-.93 1.17 1.17 0 0 1 .39-.89L21.1 16z" fill="#FF173A" data-name="Layer 1"/></svg>',
		infinite: true,
		// responsive: [
		// 	{
		// 		breakpoint: 800,
		// 		settings: {
		// 			slidesToShow: 1,
		// 		}
		// 	},
		// ]
	});

	/*------------------------------------------------------------------
  	DISPLAY DAY FOR LEAGUES
	------------------------------------------------------------------*/
	$('#league h3+div:empty').prev().remove();
	$('#league div:empty').remove();

	/*------------------------------------------------------------------
  	RELOAD LEAGUES PAGE ON INPUT SELECT
	------------------------------------------------------------------*/
	$('body.post-type-archive-league main article > div form input[type=radio][name="location"]').on('change', function() {
    $(this).closest("form").submit();
});

});